require('dotenv').config();

const chalk = require('chalk');
const debug = require('debug')('app');

const FolderWatcher = require('./src/services/folder-watcher');
// const Box = require('./src/services/box');
const FileOperations = require('./src/services/file-operations');
// const Slack = require('./src/services/slack');

const folderWatcher = new FolderWatcher();
// let box = new Box();
const fileops = new FileOperations();
// let slack = new Slack();

folderWatcher.startWatching((err, event, path) => {
  if (err) {
    debug(chalk.red(`${(err)}`));
  } else if (event === 'Added') {
    debug(chalk.green(`${(path)} ${(event)}`));
    // slack.notify('Folder Poller', 'green', `${(path)} ${(event)}`);
  } else if (event === 'Unlinked') {
    debug(chalk.keyword('orange')(`${(path)} ${(event)}`));
    // slack.notify('Folder Poller', 'green', `${(path)} ${(event)}`);
    fileops.unlinkfile();
  }
});
