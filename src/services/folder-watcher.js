const chokidar = require('chokidar');

function folderWatcher() {
  // Initialize watcher.
  const watcher = chokidar.watch('./folder', {
    ignored: /[\/\\]\./,
    persistent: true
  });

  const startWatching = (cb) => {
    watcher
      .on('add', (path) => {
        cb(null, 'Added', path);
      })
      .on('unlink', (path) => {
        cb(null, 'Unlinked', path);
      })
      .on('error', (error) => {
        cb(error, 'Error', null);
      });
  };

  const stopWatching = () => {
    watcher.close();
  };

  return {
    startWatching,
    stopWatching
  };
}
module.exports = folderWatcher;
